package com.demo.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

// 订单事件数据实体类
public class OrderEvent implements Serializable {
    private Long userId;
    private String action;
    private String orId;
    private Long timestamp;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getOrId() {
        return orId;
    }

    public void setOrId(String orId) {
        this.orId = orId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public OrderEvent(Long userId, String action, String orId, Long timestamp) {
        this.userId = userId;
        this.action = action;
        this.orId = orId;
        this.timestamp = timestamp;
    }

    public OrderEvent() {
    }

    @Override
    public String toString() {
        return "OrderLog{" +
                "userId=" + userId +
                ", action='" + action + '\'' +
                ", orId='" + orId + '\'' +
                ", timestamp=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timestamp)) +
                '}';
    }
}
