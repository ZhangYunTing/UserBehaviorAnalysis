package com.demo.bean;

import java.io.Serializable;

public class OrderLogEvent12 implements Serializable {
    private Long orderId;
    private Double amount;
    private Long timeStamp;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public OrderLogEvent12(Long orderId, Double amount, Long timeStamp) {
        this.orderId = orderId;
        this.amount = amount;
        this.timeStamp = timeStamp;
    }

    public OrderLogEvent12() {
    }


    @Override
    public String toString() {
        return "OrderLogEvent1{" +
                "orderId=" + orderId +
                ", amount=" + amount +
                ", timeStamp=" + timeStamp +
                '}';
    }


}
