package com.demo.bean;

import java.io.Serializable;

public class OrderLogEvent2  implements Serializable {
    private long orderId;
    private long itemId;
    private long timeStamp;

    @Override
    public String toString() {
        return "OrderLogEvent2{" +
                "orderId=" + orderId +
                ", itemId=" + itemId +
                ", timeStamp=" + timeStamp +
                '}';
    }

    public OrderLogEvent2() {

    }

    public OrderLogEvent2(long orderId, long itemId, long timeStamp) {
        this.orderId = orderId;
        this.itemId = itemId;
        this.timeStamp = timeStamp;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
