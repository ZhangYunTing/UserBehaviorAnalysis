package com.demo.bean;

import java.io.Serializable;

public class OrderLogEvent3 implements Serializable {
    private Long orderId;
    private Double amount;
    private Long itemId;

    @Override
    public String toString() {
        return "OrderLogEvent3{" +
                "orderId=" + orderId +
                ", amount=" + amount +
                ", itemId=" + itemId +
                '}';
    }

    public OrderLogEvent3(Long orderId, Double amount, Long itemId) {
        this.orderId = orderId;
        this.amount = amount;
        this.itemId = itemId;
    }

    public OrderLogEvent3() {
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }
}
