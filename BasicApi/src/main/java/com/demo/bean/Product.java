package com.demo.bean;

import java.io.Serializable;

public class Product implements Serializable {
    private String isbn;
    private String name;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product{" +
                "isbn='" + isbn + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public Product() {


    }
    public Product(String isbn, String name) {
        this.isbn = isbn;
        this.name = name;
    }
}
