package com.demo.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

// 到账事件数据实体类
public class ReceiptEvent implements Serializable {
    private String orId;
    private String payEquipment;
    private Long timestamp;

    public ReceiptEvent(String orId, String payEquipment, Long timestamp) {
        this.orId = orId;
        this.payEquipment = payEquipment;
        this.timestamp = timestamp;
    }

    public ReceiptEvent() {
    }

    public String getOrId() {
        return orId;
    }

    public void setOrId(String orId) {
        this.orId = orId;
    }

    public String getPayEquipment() {
        return payEquipment;
    }

    public void setPayEquipment(String payEquipment) {
        this.payEquipment = payEquipment;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ReceiptLog{" +
                "orId='" + orId + '\'' +
                ", payEquipment='" + payEquipment + '\'' +
                ", timestamp=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timestamp)) +
                '}';
    }
}
