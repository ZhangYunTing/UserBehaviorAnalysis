
package com.demo.join;

import com.demo.bean.OrderLogEvent1;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * https://blog.csdn.net/wangshuminjava/article/details/104837593
 */
public class ListSerial {
    /**
     * 获取时间
     *
     * @return
     */
    public static long getTime(String timeStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            return sdf.parse(timeStr).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1L;
    }

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        ArrayList<OrderLogEvent1> list = new ArrayList<OrderLogEvent1>();
        list.add(new OrderLogEvent1(1L, 22.1, getTime("2020-04-29 13:01")));
        list.add(new OrderLogEvent1(2L, 22.2, getTime("2020-04-29 13:03")));
        list.add(new OrderLogEvent1(4L, 22.3, getTime("2020-04-29 13:04")));
        list.add(new OrderLogEvent1(4L, 22.4, getTime("2020-04-29 13:05")));
        list.add(new OrderLogEvent1(5L, 22.5, getTime("2020-04-29 13:07")));
        list.add(new OrderLogEvent1(6L, 22.6, getTime("2020-04-29 13:09")));
        //env.fromElements(new OrderLogEvent1(1L, 22.1, getTime("2020-04-29 13:01")), new OrderLogEvent1(1L, 22.1, getTime("2020-04-29 13:01"))).print("leftOrderStream");
        env.fromCollection(list.iterator(), OrderLogEvent1.class);
        //Caused by: java.io.NotSerializableException: java.util.ArrayList$Itr
        env.execute("list序列化 ???");
    }

}
