package com.example.tolerant;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * 这里主要演示：容错处理的代码步骤
 */
public class StateBackEnd {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //只有开启checkpoint 才会有重启策略: 默认checkpointing 无限重启，重启间隔8S
        env.enableCheckpointing(8000L);
        //设置重启策略为2次，间隔2秒--理解上是最多支持2次异常的重启。第二次后就挂了程序退出。疑问，另外几种重启策略
        //env.setRestartStrategy(RestartStrategies.fixedDelayRestart(2, 2));
        DataStreamSource<String> dataStreamSource = env.socketTextStream("111.230.113.15", 9090);
        dataStreamSource.map(new MapFunction<String, Tuple2<String, Integer>>() {
            @Override
            public Tuple2<String, Integer> map(String value) throws Exception {
                if ("jerry".equals(value)) {
                    throw new RuntimeException("输入异常触发词 jerry");
                }
                return new Tuple2<String, Integer>(value, 1);
            }
        }).keyBy(0).sum(1).print();
        env.execute("容错恢复测试");
    }
}
