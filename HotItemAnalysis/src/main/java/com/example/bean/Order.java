package com.example.bean;

public class Order {
    private String orderid;
    private Long quantity;

    public Order(String orderid, Long quantity) {
        this.orderid = orderid;
        this.quantity = quantity;
    }

    public Order() {

    }

    @Override
    public String toString() {
        return "Order{" +
                "orderid='" + orderid + '\'' +
                ", quantity=" + quantity +
                '}';
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
