package com.example.bean;

public class WaterC {
    public String code;
    public long time;

    @Override
    public String toString() {
        return "WaterC{" +
                "code='" + code + '\'' +
                ", time=" + time +
                '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }


    public WaterC(String code, long time) {
        this.code = code;
        this.time = time;
    }

    public WaterC() {
    }


}
