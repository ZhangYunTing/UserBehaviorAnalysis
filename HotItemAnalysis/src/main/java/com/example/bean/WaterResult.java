package com.example.bean;

public class WaterResult {
    public String key;
    public long eventTime;
    public String currentMaxTimestamp;
    public String watermark;
    public String windowStart;
    public String windowEnd;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getEventTime() {
        return eventTime;
    }

    public void setEventTime(long eventTime) {
        this.eventTime = eventTime;
    }

    public String getCurrentMaxTimestamp() {
        return currentMaxTimestamp;
    }

    public void setCurrentMaxTimestamp(String currentMaxTimestamp) {
        this.currentMaxTimestamp = currentMaxTimestamp;
    }

    public String getWatermark() {
        return watermark;
    }

    public void setWatermark(String watermark) {
        this.watermark = watermark;
    }

    public String getWindowStart() {
        return windowStart;
    }

    public void setWindowStart(String windowStart) {
        this.windowStart = windowStart;
    }

    public String getWindowEnd() {
        return windowEnd;
    }

    public void setWindowEnd(String windowEnd) {
        this.windowEnd = windowEnd;
    }


    @Override
    public String toString() {
        return "WaterResult{" +
                "key='" + key + '\'' +
                ", eventTime=" + eventTime +
                ", currentMaxTimestamp='" + currentMaxTimestamp + '\'' +
                ", watermark='" + watermark + '\'' +
                ", windowStart='" + windowStart + '\'' +
                ", windowEnd='" + windowEnd + '\'' +
                '}';
    }


    public WaterResult(String key, long eventTime, String currentMaxTimestamp, String watermark, String windowStart, String windowEnd) {
        this.key = key;
        this.eventTime = eventTime;
        this.currentMaxTimestamp = currentMaxTimestamp;
        this.watermark = watermark;
        this.windowStart = windowStart;
        this.windowEnd = windowEnd;
    }

    public WaterResult() {
    }
}
