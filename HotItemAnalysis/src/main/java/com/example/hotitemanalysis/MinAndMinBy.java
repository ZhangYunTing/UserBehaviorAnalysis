package com.example.hotitemanalysis;

import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.ArrayList;
import java.util.List;

/**
 * 区分Min和MinBy的区别。
 * https://www.fons.com.cn/66353.html
 * The difference between min and minBy is that min returns the minimum value, whereas minBy returns the element
 * that has the minimum value in this field (same for max and maxBy).
 * min和minBy之间的区别是min返回最小值，而minBy返回在此字段中具有最小值的元素（与max和maxBy相同）。
 *
 *
 * <p>
 * 但是事实上,min与max 也会返回整个元素。
 * <p>
 * 不同的是min会根据指定的字段取最小值，并且把这个值保存在对应的位置上，对于其他的字段取了最先获取的值，不能保证每个元素的数值正确，max同理。
 * <p>
 * 而minBy会返回指定字段取最小值的元素，并且会覆盖指定字段小于当前已找到的最小值元素。maxBy同理。
 */
public class MinAndMinBy {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //获取数据源
        List data = new ArrayList<Tuple3<Integer, Integer, Integer>>();
        data.add(new Tuple3<>(0, 2, 2));
        data.add(new Tuple3<>(0, 1, 1));
        data.add(new Tuple3<>(0, 5, 6));
        data.add(new Tuple3<>(0, 3, 5));
        data.add(new Tuple3<>(1, 1, 9));
        data.add(new Tuple3<>(1, 2, 8));
        data.add(new Tuple3<>(1, 3, 10));
        data.add(new Tuple3<>(1, 2, 9));
        DataStreamSource<Tuple3<Integer, Integer, Integer>> items = env.fromCollection(data);
        items.keyBy(0).min(2).print("min");
        items.keyBy(0).min(2).print("minBy");
        env.execute("defined streaming source");
    }

}
