package com.example.hotitemanalysis;

import com.example.bean.Order;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.net.URL;

public class OrderReBalanceAnalysis {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(4);

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        URL url = OrderReBalanceAnalysis.class.getResource("/Order.csv");
        DataStream<String> inputStream = env.readTextFile(url.getPath());
        SingleOutputStreamOperator<Order> dataStream = inputStream.map(new MapFunction<String, Order>() {
            @Override
            public Order map(String line) throws Exception {
                return new Order(line.split(",")[0], Long.valueOf(line.split(",")[1]));
            }
        });
        dataStream.print("dataStream");

        dataStream.keyBy((KeySelector<Order, String>) order -> order.getOrderid())
                .print("keyByStream");

        dataStream.shuffle().print("shuffleStream");

        dataStream.rebalance().print("rebalance");

        dataStream.global().print("global");

        env.execute("job- OrderReBalanceAnalysis");
    }

}
