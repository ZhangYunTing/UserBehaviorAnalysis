package com.example.hotitemanalysis;

import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.TableEnvironment;
import org.apache.flink.table.api.java.StreamTableEnvironment;

/**
 * 时间语义和窗口计算
 */
public class TimeAndWindowAnalysis {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // 从调用时刻开始给env创建的每一个stream追加时间特征
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        TableEnvironment tabEnv = StreamTableEnvironment.create(env);
        DataStreamSource<String> dataStream = env.readTextFile(TimeAndWindowAnalysis.class.getResource("/SensorReading.csv").getPath());
        env.execute("job TimeAndWindowAnalysis");
    }
}
