package com.example.networkflow;

public class ApacheLogEvent {

    private String ip;
    private String userId;
    private Long tiemstamp;
    private String method;
    private String url;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTiemstamp() {
        return tiemstamp;
    }

    public void setTiemstamp(Long tiemstamp) {
        this.tiemstamp = tiemstamp;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "com.example.networkflow.ApacheLogEvent{" +
                "ip='" + ip + '\'' +
                ", userId='" + userId + '\'' +
                ", tiemstamp=" + tiemstamp +
                ", method='" + method + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public ApacheLogEvent() {
    }

    public ApacheLogEvent(String ip, String userId, Long tiemstamp, String method, String url) {
        this.ip = ip;
        this.userId = userId;
        this.tiemstamp = tiemstamp;
        this.method = method;
        this.url = url;
    }
}
