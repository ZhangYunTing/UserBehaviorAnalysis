package com.lelebd.szt.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 站点日统计流量
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StationCount {
    //交易时间，格式化的日期时间 			2018-09-01
    private String deal_date;
    //站名								74路|华强北
    private String station;
    //人流统计                            2
    private long count;

}
