package com.lelebd.szt.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Swipe {
    //交易时间，格式化的日期时间 			2018-09-01 11:19:04
    private String deal_date;
    //结算时间，次日起始时间0点				2018-09-01 00:00:00
    private String close_date;
    //卡号，9位字母						FFEBEBAJH
    private String card_no;
    //交易金额，整数分值，原价				200
    private String deal_value;
    //交易类型，汉字描述 					地铁入站|地铁出站|巴士
    private String deal_type;
    //公司名称，线名						巴士集团|地铁七号线
    private String company_name;
    //车号								01563D|宽AGM26-27
    private String car_no;
    //站名								74路|华强北
    private String station;
    //联程标记							0 直达 |1 联程
    private String conn_mark;
    //实收金额，整数分值，优惠后			    190
    private String deal_money;
    //闸机号								265030122
    private String equ_no;
}
