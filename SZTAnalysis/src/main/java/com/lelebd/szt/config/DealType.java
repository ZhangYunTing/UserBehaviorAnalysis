package com.lelebd.szt.config;

public enum DealType {
    ENTER("地铁入站"),
    LEAVE("地铁出站");
    private String name;

    DealType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
