package com.lelebd.szt.data;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 类名/接口名
 *
 * @author zhangyunfei
 * @date 2022年05月10日 13:15
 */
@Slf4j
public class LogBackTest {
    private final static Logger logger = LoggerFactory.getLogger(LogBackTest.class);

    public static void main(String[] args) {
        log.info("~~~~~~~~~~log");
        logger.info("~~~~~~~~~~log");
    }
}
